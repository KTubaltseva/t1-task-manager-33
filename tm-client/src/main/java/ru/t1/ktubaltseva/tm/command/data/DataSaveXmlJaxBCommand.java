package ru.t1.ktubaltseva.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.data.DataSaveXmlJaxBRequest;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public class DataSaveXmlJaxBCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-save-xml-jaxb";

    @NotNull
    private final String DESC = "Save data to xml file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SAVE XML DATA]");
        @NotNull final DataSaveXmlJaxBRequest request = new DataSaveXmlJaxBRequest(getToken());
        getDomainEndpoint().saveDataXmlJaxB(request);
    }

}
