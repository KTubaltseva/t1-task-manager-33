package ru.t1.ktubaltseva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.project.ProjectClearRequest;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-clear";

    @NotNull
    private final String DESC = "Clear project list.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[PROJECT CLEAN]");
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getToken());
        getProjectEndpoint().clear(request);
    }

}
