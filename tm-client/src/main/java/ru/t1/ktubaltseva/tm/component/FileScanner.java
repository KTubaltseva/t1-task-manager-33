package ru.t1.ktubaltseva.tm.component;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.command.AbstractCommand;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FileScanner {

    @NotNull
    private final ScheduledExecutorService es =
            Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final File folder = new File("./");

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        @NotNull final Iterable<AbstractCommand> abstractCommands =
                bootstrap.getCommandService().getSystemCommands();
        abstractCommands.forEach(command -> this.commands.add(command.getName()));
        es.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void process() {
        for (@NotNull final File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            @NotNull final String filename = file.getName();
            final boolean check = commands.contains(filename);
            if (check) {
                try {
                    file.delete();
                    bootstrap.processCommand(filename);
                } catch (@NotNull final AbstractException | NoSuchAlgorithmException | JsonProcessingException e) {
                    bootstrap.getLoggerService().error(e);
                }
            }
        }
    }

}
