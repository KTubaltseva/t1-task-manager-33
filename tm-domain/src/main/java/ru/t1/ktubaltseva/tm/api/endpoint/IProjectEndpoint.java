package ru.t1.ktubaltseva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.project.*;
import ru.t1.ktubaltseva.tm.dto.response.project.*;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import static ru.t1.ktubaltseva.tm.api.endpoint.IEndpoint.*;

@WebService
public interface IProjectEndpoint {

    @NotNull
    String NAME = "ProjectEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IProjectEndpoint.class);
    }

    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(
            @NotNull final String host,
            @NotNull final String port
    ) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IProjectEndpoint.class);
    }

    @NotNull
    @WebMethod
    ProjectChangeStatusByIdResponse changeStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectChangeStatusByIndexResponse changeStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectClearResponse clear(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectClearRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectCompleteByIdResponse completeById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectCompleteByIndexResponse completeByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectCreateResponse create(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCreateRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectDisplayByIdResponse getById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectDisplayByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectDisplayByIndexResponse getByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectDisplayByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectDisplayListResponse getAll(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectDisplayListRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectRemoveByIdResponse removeById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectRemoveByIndexResponse removeByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectStartByIdResponse startById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectStartByIndexResponse startByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectUpdateByIdResponse updateById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectUpdateByIndexResponse updateByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIndexRequest request
    ) throws AbstractException;

}
