package ru.t1.ktubaltseva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.task.*;
import ru.t1.ktubaltseva.tm.dto.response.task.*;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import static ru.t1.ktubaltseva.tm.api.endpoint.IEndpoint.*;

@WebService
public interface ITaskEndpoint {

    @NotNull
    String NAME = "TaskEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, ITaskEndpoint.class);
    }

    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance(
            @NotNull final String host,
            @NotNull final String port
    ) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, ITaskEndpoint.class);
    }

    @NotNull
    @WebMethod
    TaskBindToProjectResponse bindToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskBindToProjectRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskChangeStatusByIdResponse changeStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskChangeStatusByIndexResponse changeStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskClearResponse clear(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskClearRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskCompleteByIdResponse completeById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCompleteByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskCompleteByIndexResponse completeByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCompleteByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskCreateResponse create(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCreateRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskDisplayByIdResponse getById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskDisplayByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskDisplayByIndexResponse getByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskDisplayByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskDisplayByProjectIdResponse getByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskDisplayByProjectIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskDisplayListResponse getAll(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskDisplayListRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskRemoveByIdResponse removeById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskRemoveByIndexResponse removeByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskStartByIdResponse startById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskStartByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskStartByIndexResponse startByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskStartByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskUnbindFromProjectResponse unbindFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUnbindFromProjectRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskUpdateByIdResponse updateById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskUpdateByIndexResponse updateByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIndexRequest request
    ) throws AbstractException;

}
