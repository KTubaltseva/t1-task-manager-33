package ru.t1.ktubaltseva.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractResultResponse extends AbstractResponse {

    @NotNull
    private Boolean success = true;

    @Nullable
    private String message = "";

    public AbstractResultResponse(@NotNull final Throwable throwable) {
        this.success = false;
        this.message = throwable.getMessage();
    }
}
