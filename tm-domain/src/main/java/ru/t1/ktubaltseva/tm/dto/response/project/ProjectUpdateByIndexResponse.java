package ru.t1.ktubaltseva.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
public class ProjectUpdateByIndexResponse extends AbstractProjectResponse {

    public ProjectUpdateByIndexResponse(@NotNull final Project project) {
        super(project);
    }

}
