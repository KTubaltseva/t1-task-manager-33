package ru.t1.ktubaltseva.tm.dto.response.system;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationVersionResponse extends AbstractResponse {

    @Nullable
    private String version;


}
