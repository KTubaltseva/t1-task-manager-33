package ru.t1.ktubaltseva.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public class TaskDisplayByIdResponse extends AbstractTaskResponse {

    public TaskDisplayByIdResponse(@NotNull final Task task) {
        super(task);
    }

}
