package ru.t1.ktubaltseva.tm.exception.entity.entityNotFound;

public final class UserNotFoundException extends AbstractEntityNotFoundException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}
