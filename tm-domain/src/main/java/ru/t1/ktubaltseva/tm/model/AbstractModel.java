package ru.t1.ktubaltseva.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

@Getter
@NoArgsConstructor
public class AbstractModel implements Serializable {

    @NotNull
    private final String id = UUID.randomUUID().toString();

}
