package ru.t1.ktubaltseva.tm.api.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.AbstractRequest;
import ru.t1.ktubaltseva.tm.dto.response.AbstractResponse;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import java.security.NoSuchAlgorithmException;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    @NotNull
    RS execute(RQ request) throws AbstractException, NoSuchAlgorithmException;

}
