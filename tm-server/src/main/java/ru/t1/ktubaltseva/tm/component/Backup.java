package ru.t1.ktubaltseva.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.service.DomainService;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup {

    @NotNull
    private final ScheduledExecutorService es =
            Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void load() throws AbstractException, NoSuchAlgorithmException {
        if (!Files.exists(Paths.get(DomainService.FILE_BACKUP))) return;
        bootstrap.getDomainService().loadDataBackup();
    }

    public void save() {
        try {
            bootstrap.getDomainService().saveDataBackup();
        } catch (@NotNull final AbstractException e) {
            bootstrap.getLoggerService().error(e);
        }
    }

    public void start() {
        try {
            load();
            es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
        } catch (@NotNull final AbstractException | NoSuchAlgorithmException e) {
            bootstrap.getLoggerService().error(e);
        }
    }

    public void stop() {
        es.shutdown();
    }

}
